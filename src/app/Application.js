import config from '../config';
import EventEmitter from 'eventemitter3';

const EVENTS = {
  APP_READY: 'app_ready',
};

/**
 * App entry point.
 * All configurations are described in src/config.js
 */
export default class Application extends EventEmitter {
  constructor() {
    super();
    
    this.config = config;
    this.data = { count: 0, planets: []};

    this.init();
  }

  static get events() {
    return EVENTS;
  }

  /**
   * Initializes the app.
   * Called when the DOM has loaded. You can initiate your custom classes here
   * and manipulate the DOM tree. Task data should be assigned to Application.data.
   * The APP_READY event should be emitted at the end of this method.
   */   
  async init() {
    // Initiate classes and wait for async operations here.
    

	Application.data = { count: 0, planets: [] };

	// There are still pages with data when the data.next property is different from null
	var availablePages = true;
	//Page number
	var number = 1;
	while(availablePages){
		// Fetching the Planets and their count from the url
		await fetch(`https://swapi.boom.dev/api/planets/?page=${number}?format=json`)
		.then(response => response.json())
		.then(data => {
				if(data.next === null)
					availablePages = false; // no more pages with planets available
				// Storing the count and planets array in the Application.data object
				Application.data.count = data.count; // Planet count
				Application.data.planets.push(...data.results); // Adding planets from each page into single array
				number++;
		});
		
	}
	this.data = Application.data;
	
	this.emit(Application.events.APP_READY);
  }
}

